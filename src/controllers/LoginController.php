<?php
/**
 * @desc    LoginController class file.
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/8/3
 * Time: 10:29
 */

namespace Biqu\sign_in\controllers;


use Biqu\sign_in\models\LoginForm;
use yii\base\Exception;
use yii\web\Controller;

class LoginController extends Controller {

    /**
     * @var \Biqu\sign_in\components\SignIn
     */
    private $_component;

    public function init(){
        if(empty(\Yii::$app->components['SignIn']))
            throw new Exception('Unable to find SignIn. Please check if the component is installed correctly.');
        $this->_component = \Yii::$app->SignIn;
        $this->enableCsrfValidation = FALSE;
        \Yii::setAlias('@sign_in', dirname(dirname(__FILE__)) . '/');
    }

    public function actions(){
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'minLength' => 4,
                'maxLength' => 4,
            ],
        ];
    }

    /**
     * Local Login Service
     * @return string|\yii\web\Response
     * @throws Exception
     */
    public function actionIndex(){

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        $model->scenario = 'local';
        if(\Yii::$app->request->isPost){
            if(!$this->_component->enableLocal)
                throw new Exception('Local login service is not enabled.');
            if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
        }
        return $this->render('@sign_in/views/login/index', [
            'model' => $model,
        ]);
    }

    /**
     * Local Logout Service
     * @return \yii\web\Response
     */
    public function actionLogout(){
        \Yii::$app->user->logout();
        setcookie('gbac_footstep_menu', null, -1, '/');
        setcookie('gbac_footstep_url', '', -1, '/');
        return $this->goHome();
    }
}