<?php
namespace Biqu\sign_in\models;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $captcha;
    public $rememberMe = true;

    /**
     * @var \backend\models\AdminUsers
     */
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', /*'captcha'*/], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            ['username', 'validateUsername'],
            // password is validated by validatePassword()
            ['password', 'validatePassword', 'on'=>'local'],
            // captcha is validated by build-in CaptchaValidator
//            ['captcha', 'captcha', 'captchaAction'=>'login/captcha', 'on'=>'local'],
        ];
    }

    public function scenarios(){
        return [
            'local' => ['username', 'password', 'rememberMe', 'captcha'],
        ];
    }

    public function attributeLabels(){
        return [
            'username' => '用户名',
            'password' => '密码',
            'captcha' => '验证码',
            'rememberMe' => '记住登录',
        ];
    }

    /**
     * 验证密码是否正确
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params){
        if($this->hasErrors()){
            return false;
        }
        $password = $this->$attribute;
        $user = $this->getUser();
        $res = $user->validatePassword($password);
        if($res === NULL){
            $this->addError($attribute, '该用户尚未设置密码，无法本地登录！');
            return false;
        }
        if($res === true){
            return true;
        }
        $this->addError($attribute, '密码不正确！');
        return false;
    }

    /**
     * 验证用户名有效性
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateUsername($attribute, $params){
        $user = $this->getUser();
        if(!$user){
            $this->addError('username', '您没有权限进入该后台，请与管理员联系！');
            return false;
        }
        if($user->status != \backend\models\AdminUsers::STATUS_ACTIVE){
            $this->addError('username', '该用户在本地系统尚未激活，请与管理员联系！');
            return false;
        }
        return true;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->updateLoginInfo(\common\helpers\FrontHelper::getClientIp(true), time());       //记录最近登录信息
            return Yii::$app->user->login($user, 3600 * 24 * 30);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return \backend\models\AdminUsers|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = \backend\models\AdminUsers::findIdentity($this->username);
            if($this->user){
                $this->_user->scenario = 'login';
            }
        }
        return $this->_user;
    }
}
