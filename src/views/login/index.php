<?php
\yii\bootstrap\BootstrapPluginAsset::register($this);
\Biqu\sign_in\assets\JsMd5Asset::register($this);
$this->registerJs("
    $('#login-form').submit(function(){
        var pwd = $('#loginform-password').val();
        if(pwd.length != 32){
            $('#loginform-password').val(md5(pwd));
        }
        return true;
    });
", static::POS_READY);
$this->title = '欢迎登录'.Yii::$app->name;
?>
<style>
    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
</style>
<div class="login-box">
    <div class="login-logo" style="font-size: 28px;">
        <a href="/"><?= Yii::$app->name;?></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body nav-tabs-custom">
        <ul id="loginTab" class="nav nav-tabs" role="tablist">
            <?php if(Yii::$app->SignIn->enableLocal):?>
                <li role="presentation" class="active"><a href="#tab_local_login" aria-controls="tab_local_login" role="tab" data-toggle="tab">本地登录</a></li>
            <?php endif; ?>
        </ul>
        <div class="tab-content" style="margin-top: 10px;">
        <?php if(Yii::$app->SignIn->enableLocal):?>
            <div id="tab_local_login" role="tabpanel" class="tab-pane active">
                <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'login-form', 'options'=>['class'=>'form-signin']]); ?>
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'username', ['template'=>"{input}\n{hint}\n{error}"])->input('text', ['class'=>'form-control', 'autofocus'=>'', 'placeholder'=>'用户名']); ?>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <?= $form->field($model, 'password', ['template'=>"{input}\n{hint}\n{error}"])->passwordInput(['class'=>'form-control', 'placeholder'=>'密码', 'style'=>'margin-top: 15px;']); ?>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">登录</button>
                        </div>
                        <!-- /.col -->
                    </div>
                <?php \yii\widgets\ActiveForm::end(); ?>
            </div>
        <?php endif;?>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>