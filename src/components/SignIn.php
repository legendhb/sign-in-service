<?php
/**
 * @desc    The SignIn Component, support Local-Login & Sso-Login Service.
 * @author  LegendHB<legendhb@gmail.com>
 * Date: 2015/8/5
 * Time: 10:31
 */

namespace Biqu\sign_in\components;


use yii\base\Component;
use yii\base\Exception;

class SignIn extends Component {

    public $enableLocal;

    public function init(){
        if(!$this->enableLocal)
            throw new Exception('At least one of enableLocal and enableSso must be set to true.');
    }

} 