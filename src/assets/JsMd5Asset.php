<?php

namespace Biqu\sign_in\assets;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the [md5 javascript library]
 *
 * @author LegendHB<legendhb@gmail.com>
 */
class JsMd5Asset extends AssetBundle
{
    public $sourcePath = '@sign_in/assets/';
    public $js = [
        'md5.min.js',
    ];
}
